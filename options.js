function saveOptions(e) {
  e.preventDefault();
  browser.storage.sync.set({
    target: document.querySelector("#url").value
  });
}

function restoreOptions() {

  function setCurrentChoice(result) {
    document.querySelector("#url").value = result.target || "test.com";
  }

  function onError(error) {
    console.log(`Error: ${error}`);
  }

  var getting = browser.storage.sync.get("target");
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
