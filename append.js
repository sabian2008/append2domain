function onError(error) {
  // Log error
  console.log(`Error: ${error}`);
}


function append(tab){
  // Load options at invocation to account for settings changes
  // and  then prepend
  var getting = browser.storage.sync.get("target");
  getting.then(options => openPage(tab,options), onError);

}

function openPage(tab,options) {
  var prefix = tab.url.split("/")[2]
  var suffix = tab.url.split("/").slice(3).join("/")
  var dest = "http://" + prefix + "." + options.target + "/" + suffix
  console.log(prefix);
  console.log(suffix);
  console.log(dest);
  browser.tabs.create({
    url: dest});
}


// Add listener to extension button
browser.browserAction.onClicked.addListener(append);

